import 'package:flutter/material.dart';
enum APP_THEME{LIGHT, DARK}

void main() {
  runApp( ContactProfilePage());
}
class MyApptheme{
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.white,
            iconTheme: IconThemeData(
              color: Colors.black,
            )
        ),
        iconTheme: IconThemeData(
            color: Colors.indigo[900]
        )
    );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.black,
            iconTheme: IconThemeData(
              color: Colors.pink[300],
            )
        ),
        iconTheme: IconThemeData(
            color: Colors.pink[300]
        )
    );
  }
}
class ContactProfilePage extends StatefulWidget{
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}
class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyApptheme.appThemeLight()
          : MyApptheme.appThemeDark(),
      home: Scaffold(
          appBar: buildAppWidget() ,
          body: buildBodyWidget(),
          floatingActionButton: FloatingActionButton(
            // onPressed: () {
            //   setState(() {
            //     currentTheme == APP_THEME.DARK
            //         ? currentTheme == APP_THEME.LIGHT
            //         : currentTheme == APP_THEME.DARK;
            //   });
            // },
            // child: Icon(Icons.tungsten),
            child: Icon(Icons.tungsten_outlined),
                onPressed: () {
                setState(() {
                  currentTheme == APP_THEME.DARK
                      ? currentTheme = APP_THEME.LIGHT
                      : currentTheme = APP_THEME.DARK;
                });
            },
            ),
          ),
      );

  }
}
AppBar buildAppWidget(){
   return AppBar(
   // backgroundColor: Colors.indigo[300],
    leading: Icon(Icons.arrow_back),
    actions: <Widget>[
      IconButton(onPressed: (){}, icon: Icon(Icons.star_border), )
    ],
  );
}
ListView buildBodyWidget(){
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child: Image.network(
              "https://f.ptcdn.info/945/072/000/qqghnzely7m56cCse12-o.jpg",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Soisawan Imwibul", //Rimuru Tempest
                    style: TextStyle(fontSize:30,),
                  ),
                )
              ],
            ),
          ),
          Divider(
            color: Colors.indigo[900],
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.pink[300],
                )
              ),
              child: profileActionItem(),
            ),
          ),
          Divider(
            color: Colors.indigo[900],
          ),
          mobilePhoneLiteTile(),
          otherPhoneLiteTile(),
          Divider(
            color: Colors.indigo[900],
          ),
          emailLiteTile(),
          Divider(
            color: Colors.indigo[900],
          ),
          addressLiteTile(),

        ],
      ),

    ],
  );

}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
       //   color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}
Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call_sharp,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email_sharp,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}
Widget buildDirectionButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Payment"),
    ],
  );
}
//ListTile
Widget mobilePhoneLiteTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("099-508-6978"),
    subtitle: Text("mobile"),
    trailing: IconButton(
        icon:Icon(Icons.message),
     // color: Colors.indigo.shade800,
      onPressed: () {},
    ),
  );
}
Widget otherPhoneLiteTile(){
  return ListTile(
    leading: Text(""),
    title: Text("099-615-5192"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon:Icon(Icons.message),
     // color: Colors.indigo.shade800,
      onPressed: () {},
    ),
  );
}
Widget emailLiteTile(){
  return ListTile(
    leading: Icon(Icons.email_sharp),
    title: Text("63160225@go.buu.ac.th"),
    subtitle: Text("work"),
    trailing: IconButton(
      icon:Icon(Icons.message),
    //  color: Colors.indigo.shade800,
      onPressed: () {},
    ),
  );
}
Widget addressLiteTile(){
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("177 Rimuru House"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon:Icon(Icons.message),
     // color: Colors.indigo.shade800,
      onPressed: () {},
    ),
  );
}
Widget profileActionItem(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionButton(),
      buildPayButton()
    ],
  );
}


